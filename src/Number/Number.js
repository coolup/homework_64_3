import React, { Component } from 'react';
import './Number.css';

class Number extends Component {
    
    state = {
        numbers: this.getRandomArray(5, 36)
    };
    getRandomArray(min, max) {
        let randomArray = [];
        for (var i = 0; randomArray.length <= 4; i++) {
            let randomNum = Math.floor(Math.random() * (max - min + 1)) + min;
            if (randomArray.indexOf(randomNum) > -1)
                continue;
                randomArray.push(randomNum)
        }
        return randomArray.sort(function (x, y) {
            return x - y;
        });              
    }
    changeNumbers = () => {
        this.setState({
            numbers: this.getRandomArray(5, 36)
        })
    }

    render() {
        return (
            <div className="container">
                <input type="button" value="New numbers" onClick={this.changeNumbers} />
                <div className="numbers">
                    <div className="number">
                        <p>{this.state.numbers[0]}</p>
                    </div>
                    <div className="number">
                        <p>{this.state.numbers[1]}</p>
                    </div>
                    <div className="number">
                        <p>{this.state.numbers[2]}</p>
                    </div>
                    <div className="number">
                        <p>{this.state.numbers[3]}</p>
                    </div>
                    <div className="number">
                        <p>{this.state.numbers[4]}</p>
                    </div>
                </div>
            </div>
        );
    }
};
export default Number;